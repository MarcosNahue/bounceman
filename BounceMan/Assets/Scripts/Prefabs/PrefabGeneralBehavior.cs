using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabGeneralBehavior : MonoBehaviour
{

    private float myPowerDuration;
    private GameObject myPlayer;
    protected float powerDuration
    {
        get { return myPowerDuration; }
        set { myPowerDuration = value; }
    }

    protected GameObject oPlayer
    {
        get { return myPlayer; }
        set { myPlayer = value; }
    }



    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
    }


}
