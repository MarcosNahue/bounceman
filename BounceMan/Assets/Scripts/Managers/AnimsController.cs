using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimsController : MonoBehaviour
{
    Animator oAnimator;
    [SerializeField] GameObject oPlayer;
    Animation oPogoAnimation;
    void Start()
    {
        oAnimator = this.GetComponent<Animator>();
        oPogoAnimation = oPlayer.GetComponentInChildren<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        oPogoAnimation.Play("PogoMovement");
 
    }
}
