using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float moveSpeed = 4f; 
    [SerializeField] float switchTrackTime;
    [SerializeField] float jumpSpeed;
    public int powerupCase;

    private GameObject oPlayer;
    private bool onFloor = true;
    
    void Start()
    {
        oPlayer = this.gameObject;
    }

    void Update()
    {
        oPlayer.transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed, Space.Self);
        checkInput();
    }

    void checkInput() 
    {
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) //LEFT
        {
            if (oPlayer.transform.localPosition.z < 2)
            {
                LeanTween.moveLocalZ(oPlayer, Mathf.Round(oPlayer.transform.position.z + 2), switchTrackTime);
            }       
        } 
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) //RIGHT
        {
            if (oPlayer.transform.localPosition.z >= 0)
            {
                LeanTween.moveLocalZ(oPlayer, Mathf.Round(oPlayer.transform.position.z - 2) , switchTrackTime);
            }
        }
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow)) //UP
        {
            if (onFloor)
            {
                Jump();
            }

        }
    }

    void Jump() 
    {
        onFloor = false;
        LeanTween.moveLocalY(oPlayer, Mathf.Round(oPlayer.transform.position.y + 3), jumpSpeed);
    }

    void CheckPowerupDuration() 
    {

    
    }

    void CheckPowerUp() 
    {
        switch (powerupCase)
        {
            default:
                break;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.tag == "Floor")
        {
            onFloor = true;
        }
    }

}
